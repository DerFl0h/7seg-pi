#include <stdio.h>
#include <ctime>
#include <csignal>
#include <stdlib.h> 
#include <wiringPi.h>
#include <iostream>

const int sw=2;         //Next switch
int p[4]={7,0,2,3};     //PIN of each Segment(Common Annode)
int s[8]={15,16,1,4,5,6,10,11}; //PIN of part A to G and Decimal Point
bool d[][7]=                 //{'0'="abcdef",'1'="bc",'2'="abdeg",'3'="abcdg",'4'="bcfg",'5'="acdfg",'6'="acdefg",'7'="abc",'8'="abcdefg",'9'="abcdfg",'E'="adefg",'r'="eg",'o'="cdeg",'g'="abfg",'C'="adef"};
    {
        {1,1,1,1,1,1,0},    //0
        {0,1,1,0,0,0,0},    //1
        {1,1,0,1,1,0,1},    //2
        {1,1,1,1,0,0,1},    //3        
        {0,1,1,0,0,1,1},    //4
        {1,0,1,1,0,1,1},    //5
        {1,0,1,1,1,1,1},    //6
        {1,1,1,0,0,0,0},    //7
        {1,1,1,1,1,1,1},    //8
        {1,1,1,1,0,1,1},    //9
        {1,1,0,0,0,1,1},     //°
        {1,0,0,1,1,1,0},     //C
    };
int o[4]={4,5,6,9};         //symbole to be displayed
bool dp[4]={1,1,0,1};       //Decimal points
int ja=0;                   //Year
int mo=0;                   //Month
int ta=0;                   //Day
int ho=0;                   //Hour
int min=0;                  //Minuts
unsigned long n=0;                    //Last switch time
int c=0;                    //Case

void w(){
    for(int i = 0; i < 4; i++){
        digitalWrite(p[i],HIGH);
        for(int j = 0; j < 7; j++){
            digitalWrite(s[j],!d[o[i]][j]);      
        }
        if(dp[i]){
            digitalWrite(s[7],LOW);
        }
        delay(1);
                for(int j = 0; j < 7; j++){
            digitalWrite(s[j],d[o[i]][j]);      
        }
        if(dp[i]){
            digitalWrite(s[7],HIGH);
        }
        digitalWrite(p[i],LOW);
    }    
}

void temp() {
    dp[0]=0;
    dp[1]=0;
    dp[2]=0;
    dp[3]=0;
    FILE *temperature;
    double T;
    temperature = fopen ("/sys/class/thermal/thermal_zone0/temp", "r");
    fscanf (temperature, "%lf", &T);
    T /= 1000;
    fclose (temperature);
    o[0]=T/10;    
    o[1]=T-10*o[0];    
    o[2]=10;
    o[3]=11;
}

void signalHandler( int signum ) {  
    std::cout<<"\nExit=> \n";
    for(int i = 0;i < 4; i++){
        digitalWrite(p[i],LOW);    
    }
    for(int i = 0;i < 8; i++){
        digitalWrite(s[i],LOW);    
    }
    exit(signum);
}

void gd(){
    time_t t = time(0);
    struct tm * now = localtime( & t );
    mo=now->tm_mon + 1;
    ta=now->tm_mday;
    dp[0]=0;
    dp[1]=1;
    dp[2]=0;
    dp[3]=0;
    o[0]=(ta-ta%10)/10;
    o[1]=ta-o[0]*10;       
    o[2]=(mo-mo%10)/10;
    o[3]=mo-o[2]*10; 
}

void gy(){
    time_t t = time(0);
    struct tm * now = localtime( & t );
    ja=now->tm_year + 1900; 
    dp[0]=0;
    dp[1]=0;
    dp[2]=0;
    dp[3]=0;
    o[0]=(ja-ja%1000)/1000;
    o[1]=(ja-o[0]*1000-ja%100)/100;       
    o[2]=(ja-o[0]*1000-o[1]*100-ja%10)/10;
    o[3]=ja-o[0]*1000-o[1]*100-o[2]*10; 
}

void gt(){
    time_t t = time(0);
    struct tm * now = localtime( & t );
    ho=now->tm_hour; 
    min=now->tm_min;
    dp[0]=0;
    dp[1]=1;
    dp[2]=0;
    dp[3]=0;
    o[0]=(ho-ho%10)/10;
    o[1]=ho-o[0]*10;       
    o[2]=(min-min%10)/10;
    o[3]=min-o[2]*10; 
}

int main() {    
    std::cout<<"Initialising\n";
    signal(SIGINT, signalHandler);
    if (wiringPiSetup() == -1)
        return 1;
    for(int i = 0; i<4; i++){
        pinMode(p[i],OUTPUT);  
        digitalWrite(p[i],LOW);
    }
    for(int i = 0; i<8; i++){
        pinMode(s[i],OUTPUT);  
        digitalWrite(s[i],HIGH);
    }
    
   while(true){   
        time_t t = time(0);
        w();
        if(n<t){
            c++;
            n=t+sw;
            if(c==4)c=0;
            switch(c){
            case 0:      
                gt();            
                break;
            case 1:      
                gd();
                break;
            case 2:      
                gy();
                break;
            case 3:
                temp();
                break;
        }
        }     
   }

}

